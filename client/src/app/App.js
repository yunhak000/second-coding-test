import GlobalStyles from "./GlobalStyles";
import { Reset } from "styled-reset";
import Header from "../common/components/Header";
import DocList from "../common/components/DocList";
import DocCont from "../common/components/DocCont";
import { Routes, Route } from "react-router-dom";

export default function App() {
  return (
    <>
      <Reset />
      <GlobalStyles />
      <Header />
      <Routes>
        <Route path="/" element={ <DocList /> } />
        <Route path="/docCont" element={ <DocCont /> } />
      </Routes>
    </>
  );
}
