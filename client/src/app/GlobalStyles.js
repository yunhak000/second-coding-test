import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  button {
    cursor: pointer;
  }

  textarea {
    resize: none;
  }

  a {
    color: #000;
    text-decoration: none;
  }

  .container {
    width: 1080px;
    margin: 0 auto;
  }
`;

export default GlobalStyles;
