import styled from "styled-components";

export default function DocAdd() {
  return (
    <DocAddStyle href="/docCont">+</DocAddStyle>
  );
}

const DocAddStyle = styled.a`
  position: fixed;
  bottom: 20px;
  right: 50%;
  margin-right: -550px;
  width: 60px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 40px;
  border-radius: 50%;
  border: none;
  background-color: #4785f0;
  color: #fff;
`
