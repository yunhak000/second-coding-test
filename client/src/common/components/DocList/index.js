import styled from "styled-components";
import DocAdd from "./DocAdd";
import { Link } from "react-router-dom";

export default function DocList() {
  return (
    <DocListWrap className="container">
      <Link to="/docCont">
        <div className="document-feagure">
          <span>document 1</span>
          <button>Copy Url</button>
        </div>
      </Link>
      <DocAdd />
    </DocListWrap>
  );
}

const DocListWrap = styled.div`
  padding: 30px 0;

  .document-feagure {
    position: relative;
    padding: 30px 0;
    text-align: center;
    border-radius: 15px;
    margin-bottom: 20px;
    transition: 0.3s;
    cursor: pointer;
    border: 1px solid #4785f0;

    span {
      font-weight: bold;
    }
  }

  .document-feagure:hover {
    color: #fff;
    background-color: #4785f0;
  }

  .document-feagure button {
    position: absolute;
    top: 50%;
    right: 30px;
    transform: translateY(-50%);
    padding: 10px 20px;
    border-radius: 20px;
    border: 1px solid #4785f0;
    background-color: #fff;
  }
`
