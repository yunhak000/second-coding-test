import styled from "styled-components";

export default function Header() {
  return (
    <HeaderWrap>
      <div className="header-layout container">
        <h1>Google Docs</h1>
        <div>
          <button className="google-login-button">
            <img src="img/google-logo.png" alt="구글로고" />
            <span>Sign in with Google</span>
          </button>
          {/* <span className="user-name">이윤학</span>
          <span className="bar">|</span>
          <a href="" className="logout">로그아웃</a>
          <button className="my-document">내문서</button> */}
        </div>
      </div>
    </HeaderWrap>
  );
}

const HeaderWrap = styled.div`
  padding: 25px 0;
  border-bottom: 1px solid #ebebeb;

  h1 {
    font-weight: bold;
    font-size: 28px;
  }

  .header-layout {
    display: flex;
    justify-content: space-between;
    align-items: center;

    .google-login-button {
      display: flex;
      align-items: center;
      padding: 10px 20px;
      border-radius: 30px;
      border: 1px solid #d6d3d3;
      background-color: #fff;

      img {
        width: 35px;
        margin-right: 10px;
      }

      span {
        font-weight: bold;
        color: #4785f0;
      }
    }

    .user-name {
      font-weight: bold;
    }

    .bar {
      margin: 0 15px;
      font-weight: bold;
    }

    .logout {
      color: #4785f0;
    }

    .my-document {
      margin-left: 20px;
      border-radius: 10px;
      border: none;
      padding: 10px 15px;
      color: #fff;
      background-color: #4485f4;
    }
  }
`
