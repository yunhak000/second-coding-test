import styled from "styled-components";

export default function DocCont() {
  return (
    <DocContWrap className="doc-cont-layout container">
      <div className="doc-cont-wrap">
        <div>
          <input type="text" placeholder="제목을 입력해 주세요."/>
        </div>
        <div>
          <textarea name="" placeholder="내용을 입력해 주세요."></textarea>
        </div>
      </div>
      <div className="button-wrap">
        <button className="delete">삭제</button>
        <button className="save">저장</button>
      </div>
    </DocContWrap>
  );
}

const DocContWrap = styled.div`
  padding: 30px 0;

  .doc-cont-wrap {
    padding: 20px;
    border-radius: 20px;
    background-color: #ebebeb;

    input, textarea {
      width: 100%;
      box-sizing: border-box;
      padding: 20px;
      border: none;
      border-radius: 10px;
    }

    input {
      margin-bottom: 20px;
    }

    textarea {
      height: 500px;
    }
  }

  .button-wrap {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 15px;
    padding: 0 20px;

    button {
      padding: 10px 20px;
      border: none;
      border-radius: 10px;
      color: #fff;
    }

    .delete {
      background-color: #c82d2d;
    }

    .save {
      background-color: #4785f0;
    }
  }
`
